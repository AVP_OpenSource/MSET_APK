package apks.cyberseclab.ca.et_hardware_goldfish;

import android.os.Build;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    private static final String CSTE_HARDWARE = "goldfish";

    private boolean evaded;

    public MainActivity() {
        super("ET_HARDWARE_GOLDFISH");
    }

    @Override
    protected void executeEvasionDetection() {
        this.evaded = false;
        testValue(Build.HARDWARE, CSTE_HARDWARE, "Build.HARDWARE", false);
        this.fireEvadedEvent(this.evaded);
    }

    private void testValue(String actualValue, String expectedSandboxValue, String valueName, boolean prefix)
    {
        if(prefix)
            testPrefixValue(actualValue, expectedSandboxValue, valueName);
        else
            testExactValue(actualValue, expectedSandboxValue, valueName);
    }

    private void testExactValue(String actualValue, String expectedSandboxValue, String valueName)
    {
        if(actualValue.equalsIgnoreCase(expectedSandboxValue))
        {
            log(actualValue, expectedSandboxValue, valueName, "sandbox");
            this.evaded = true;
            return;
        }
        log(actualValue, expectedSandboxValue, valueName, "real");
    }

    private void testPrefixValue(String actualValue, String expectedSandboxValue, String valueName)
    {
        if(actualValue.toLowerCase().startsWith(expectedSandboxValue.toLowerCase()))
        {
            log(actualValue, expectedSandboxValue, valueName, "sandbox");
            this.evaded = true;
            return;
        }
        log(actualValue, expectedSandboxValue, valueName, "real");
    }

    private void log(String actualValue, String expectedSandboxValue, String valueName, String result)
    {
        Log.d((MainActivity.this).activityTag, "(" + result + ") " + valueName + "=" + actualValue);
    }
}
