package apks.cyberseclab.ca.et_dns;

import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_DNS");
    }

    private static final List<String> DEFAULT_ANDROID_EMULATOR_DNS_SERVERS;
    static {
        DEFAULT_ANDROID_EMULATOR_DNS_SERVERS = new LinkedList<>();
        DEFAULT_ANDROID_EMULATOR_DNS_SERVERS.add("10.0.2.3");
        DEFAULT_ANDROID_EMULATOR_DNS_SERVERS.add("10.0.2.4");
        DEFAULT_ANDROID_EMULATOR_DNS_SERVERS.add("10.0.2.5");
        DEFAULT_ANDROID_EMULATOR_DNS_SERVERS.add("10.0.2.6");
    }

    // http://stackoverflow.com/questions/3070144/how-do-you-get-the-current-dns-servers-for-android
    @Override
    protected void executeEvasionDetection() {
        Set<String> dnsServers = new HashSet<String>();
        try {
            Class<?> SystemProperties = Class.forName("android.os.SystemProperties");
            Method method = SystemProperties.getMethod("get", new Class[] { String.class });
            for (String name : new String[] { "net.dns1", "net.dns2", "net.dns3", "net.dns4", }) {
                String value = (String) method.invoke(null, name);
                if (value != null && !"".equals(value)) {
                    dnsServers.add(value);
                    Log.d(this.activityTag, String.format("Detected dns server %s", value));
                }
            }

            this.fireEvadedEvent(DEFAULT_ANDROID_EMULATOR_DNS_SERVERS.containsAll(dnsServers));
        } catch (Exception e) {
            Log.e(this.activityTag, e.getMessage());
        }
    }
}
