package apks.cyberseclab.ca.et_build_constants;

import android.os.Build;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
	private boolean evaded = false;
    public MainActivity() {
        super("ET_BuildConstants");
    }

    private static final String CSTE_BRAND = "generic";//prefix
    private static final String CSTE_DEVICE = "generic";//prefix
    private static final String CSTE_FINGERPRINT = "generic";//prefix
    private static final String CSTE_PRODUCT = "sdk";//prefix
    private static final String CSTE_MANUFACTURER = "unknown";
    private static final String CSTE_MODEL = "sdk";//??
    private static final String CSTE_TAGS = "test-keys";
    private static final String CSTE_BOARD = "unknown";

    @Override
    protected void executeEvasionDetection()
	{
		this.evaded = false;
		testPrefixValue(Build.BRAND, CSTE_BRAND, "uild.BRAND");
		testPrefixValue(Build.DEVICE, CSTE_DEVICE, "Build.DEVICE");
		testPrefixValue(Build.FINGERPRINT, CSTE_FINGERPRINT, "Build.FINGERPRINT");
		testPrefixValue(Build.PRODUCT, CSTE_PRODUCT, "Build.PRODUCT");
		testExactValue(Build.MANUFACTURER, CSTE_MANUFACTURER, "Build.MANUFACTURER");
		testExactValue(Build.TAGS, CSTE_TAGS, "Build.TAGS");
		testExactValue(Build.BOARD, CSTE_BOARD, "Build.BOARD");
		testContainsValue(Build.MODEL, CSTE_MODEL, "Build.MODEL");

		this.fireEvadedEvent(this.evaded);
        /*this.fireEvadedEvent(Build.HARDWARE.equalsIgnoreCase(CSTE_HARDWARE) ||
                             Build.MODEL.equalsIgnoreCase(CSTE_MODEL) ||
                             Build.DEVICE.equalsIgnoreCase(CSTE_DEVICE) ||
                             Build.PRODUCT.equalsIgnoreCase(CSTE_PRODUCT) ||
                             Build.MANUFACTURER.equalsIgnoreCase(CSTE_MANUFACTURER) ||
                             Build.USER.equalsIgnoreCase(CSTE_USER) ||
                             Build.BRAND.equalsIgnoreCase(CSTE_BRAND) ||
                             Build.TAGS.equalsIgnoreCase(CSTE_TAGS) ||
                             Build.BOARD.equalsIgnoreCase(CSTE_BOARD) ||
                             Build.FINGERPRINT.toLowerCase().startsWith(CSTE_FINGERPRINT.toLowerCase()));*/
    }
	private void testExactValue(String actualValue, String expectedSandboxValue, String valueName)
	{
		if(actualValue.equalsIgnoreCase(expectedSandboxValue))
		{
			log(actualValue, expectedSandboxValue, valueName, "sandbox");
			this.evaded = true;
			return;
		}
		log(actualValue, expectedSandboxValue, valueName, "real");
	}

	private void testPrefixValue(String actualValue, String expectedSandboxValue, String valueName)
	{
		if(actualValue.toLowerCase().startsWith(expectedSandboxValue.toLowerCase()))
		{
			log(actualValue, expectedSandboxValue, valueName, "sandbox");
			this.evaded = true;
			return;
		}
		log(actualValue, expectedSandboxValue, valueName, "real");
	}

	private void testContainsValue(String actualValue, String expectedSandboxValue, String valueName)
	{
		if(actualValue.toLowerCase().contains(expectedSandboxValue.toLowerCase()))
		{
			log(actualValue, expectedSandboxValue, valueName, "sandbox");
			this.evaded = true;
			return;
		}
		log(actualValue, expectedSandboxValue, valueName, "real");
	}

	private void log(String actualValue, String expectedSandboxValue, String valueName, String result)
	{
		Log.d((MainActivity.this).activityTag, "(" + result + ") " + valueName + "=" + actualValue);
	}

}
