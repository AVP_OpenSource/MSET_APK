package apks.cyberseclab.ca.et_contacts;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {

    private static final int PERM_REQ_READ_CONTACTS = 10;

    private static final int MIN_CONTACT_COUNT = 5;


    public MainActivity() {
        super("ET_CONTACTS");
    }

    @Override
    protected void executeEvasionDetection() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_CONTACTS
            }, PERM_REQ_READ_CONTACTS);
        }

        ContentResolver cr = getContentResolver();
        Cursor curContact = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        int contactCount = curContact.getCount();


        Log.d(this.activityTag, String.format("Current contact count is %d", contactCount));

        this.fireEvadedEvent(contactCount <= MIN_CONTACT_COUNT);
    }
}
