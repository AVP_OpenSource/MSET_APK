package apks.cyberseclab.ca.et_play_store_2;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_PLAY_STORE_2");
    }

    @Override
    protected void executeEvasionDetection() {
        // return true if evaded, false otherwise
        //this.fireEvadedEvent(true);
        GoogleApiAvailability avail = GoogleApiAvailability.getInstance();
        int status = avail.isGooglePlayServicesAvailable(this);

        Log.d(this.activityTag, String.format("GooglePlayAvailability status code is %d", status));
        this.fireEvadedEvent(status != ConnectionResult.SUCCESS);
    }
}
