package apks.cyberseclab.ca.et_play_store;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.Log;

import java.util.List;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_PLAY_STORE");
    }

    private static final String DEFAULT_PLAY_STORE_PACKAGE_NAME = "com.android.vending";

    @Override
    protected void executeEvasionDetection() {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> packagedApps = this.getApplicationContext().getPackageManager().queryIntentActivities(mainIntent, 0);

        boolean found = false;
        for(ResolveInfo pack : packagedApps) {
            String pkgName = pack.activityInfo.applicationInfo.packageName;
            if(pkgName.startsWith(DEFAULT_PLAY_STORE_PACKAGE_NAME)) {
                Log.d(this.activityTag, String.format("Found package named %s", pkgName));
                found = true;
                break;
            }
        }

        if(!found) {
            Log.d(this.activityTag, String.format("No package found!"));
        }

        this.fireEvadedEvent(!found);
    }
}
