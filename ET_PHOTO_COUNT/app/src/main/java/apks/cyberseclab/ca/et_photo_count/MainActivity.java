package apks.cyberseclab.ca.et_photo_count;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;
import java.util.List;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_PHOTO_COUNT");
    }

    private static final File DCIM_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
    private static final String DEFAULT_EXTENSION = "jpg";
    private static final int EMULATOR_NUM_PHOTOS_THRESHOLD = 2;

    @Override
    protected void executeEvasionDetection() {
        Log.d(this.activityTag, String.format("DCIM directory is at %s", DCIM_DIRECTORY.getAbsolutePath()));

        List<File> dcimPhotos = this.walkFileTree(DCIM_DIRECTORY, new FileFilter() {
            @Override
            public boolean accept(File file) {
                String name = file.getName();
                return file.isFile() && file.getName().substring(name.indexOf(".") + 1, name.length()).equalsIgnoreCase(DEFAULT_EXTENSION);
            }
        });

        for(File photo : dcimPhotos) {
            Log.d(this.activityTag, photo.getAbsolutePath());
        }

        Log.d(this.activityTag, String.format("There are %d photos", dcimPhotos.size()));
        this.fireEvadedEvent(dcimPhotos.size() <= EMULATOR_NUM_PHOTOS_THRESHOLD);
        // return true if evaded, false otherwise
        //this.fireEvadedEvent(true);
    }

    private List<File> walkFileTree(File path, FileFilter ff) {
        List<File> resultFiles = new LinkedList<File>();
        for(File f : path.listFiles(ff)) {
            if(f.isDirectory()) {
                resultFiles.addAll(walkFileTree(f, ff));
            } else {
                if(ff.accept(f)) {
                    resultFiles.add(f);
                }
            }
        }

        return resultFiles;
    }
}
