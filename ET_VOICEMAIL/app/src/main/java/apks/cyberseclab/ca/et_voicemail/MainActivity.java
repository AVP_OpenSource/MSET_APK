package apks.cyberseclab.ca.et_voicemail;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {

    private static final String DEFAULT_VOICEMAILNO = "+15552175049";

    public MainActivity() {
        super("ET_XYZ");
    }

    @Override
    protected void executeEvasionDetection() {
        TelephonyManager tMgr = (TelephonyManager)this.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);

        String voicemailNo = tMgr.getVoiceMailNumber();
        Log.d(this.activityTag, String.format("Voicemail number is %s", voicemailNo));
        this.fireEvadedEvent(voicemailNo.equalsIgnoreCase(DEFAULT_VOICEMAILNO));
    }
}
