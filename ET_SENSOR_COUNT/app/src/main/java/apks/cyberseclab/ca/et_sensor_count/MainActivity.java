package apks.cyberseclab.ca.et_sensor_count;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_SENSOR_COUNT");
    }

    private static final int SENSOR_COUNT_EMULATOR_THRESHOLD = 7;

    private static final Map<Integer, String> TEST_SENSOR_TYPES = new TreeMap<Integer, String>();
    static {
        TEST_SENSOR_TYPES.put(Sensor.TYPE_ACCELEROMETER, "ACCELEROMETER");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_AMBIENT_TEMPERATURE, "AMBIENT_TEMPERATURE");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_GRAVITY, "GRAVITY");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_GYROSCOPE, "GYROSCOPE");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_LIGHT, "LIGHT");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_LINEAR_ACCELERATION, "LINEAR_ACCELERATION");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_MAGNETIC_FIELD, "MAGNETIC_FIELD");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_ORIENTATION, "ORIENTATION");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_PROXIMITY, "PROXIMITY");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_PRESSURE, "PRESSURE");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_RELATIVE_HUMIDITY, "RELATIVE_HUMIDITY");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_ROTATION_VECTOR, "ROTATION_VECTOR");
        TEST_SENSOR_TYPES.put(Sensor.TYPE_TEMPERATURE, "TEMPERATURE");
    }

    @Override
    protected void executeEvasionDetection() {
        SensorManager sMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        int sCount = 0;

        for(Map.Entry<Integer, String> sensorEntry : TEST_SENSOR_TYPES.entrySet()) {
            if(sMgr.getDefaultSensor(sensorEntry.getKey()) != null) {
                Log.d(this.activityTag,
                        String.format("Found available sensor %s (id = %d)", sensorEntry.getValue(), sensorEntry.getKey()));
                sCount++;
            }
        }

        Log.d(this.activityTag, String.format("Total of %d sensor detected", sCount));
        this.fireEvadedEvent(sCount <= SENSOR_COUNT_EMULATOR_THRESHOLD);
    }
}
