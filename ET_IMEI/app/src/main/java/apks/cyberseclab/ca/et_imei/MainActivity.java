package apks.cyberseclab.ca.et_imei;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    private static final int PERM_REQ_PHONES_STATE = 10;
    private static final String DEFAULT_EMULATOR_IMEI = "000000000000000";

    public MainActivity() {
        super("ET_IMEI");
    }

    @Override
    protected void executeEvasionDetection() {
        int permCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if(permCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_PHONE_STATE
            }, PERM_REQ_PHONES_STATE);
        }

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String devImei = tm.getDeviceId();
        Log.d(this.activityTag, String.format("Current IMEI is %s", devImei));
        this.fireEvadedEvent(devImei.equalsIgnoreCase(DEFAULT_EMULATOR_IMEI));
    }
}
