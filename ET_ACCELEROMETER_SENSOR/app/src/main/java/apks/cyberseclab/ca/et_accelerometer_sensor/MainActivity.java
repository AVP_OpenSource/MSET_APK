package apks.cyberseclab.ca.et_accelerometer_sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity implements SensorEventListener {
    public MainActivity() {
        super("ET_ACCELEROMETER_SENSOR");
    }

    private static final int NUM_SAMPLES_TO_CAPTURE = 10;
    private static final float EPSILON = 0.01f;

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private int numSamples = 0;


    private float[] samplesSensorX = new float[NUM_SAMPLES_TO_CAPTURE];
    private float[] samplesSensorY = new float[NUM_SAMPLES_TO_CAPTURE];
    private float[] samplesSensorZ = new float[NUM_SAMPLES_TO_CAPTURE];

    @Override
    protected void executeEvasionDetection() {
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        Log.d(this.activityTag, String.format("Start sampling %d samples from the accelerometer sensor ...", NUM_SAMPLES_TO_CAPTURE));
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
            return;
        }

        if(numSamples >= NUM_SAMPLES_TO_CAPTURE) {
            this.computeResult();
            return;
        }

        this.samplesSensorX[this.numSamples] = sensorEvent.values[0];
        this.samplesSensorY[this.numSamples] = sensorEvent.values[1];
        this.samplesSensorZ[this.numSamples] = sensorEvent.values[2];

        this.numSamples++;
    }

    private void computeResult() {
        this.sensorManager.unregisterListener(this, this.accelerometer);
        float varSamplesX = computeVariance(this.samplesSensorX);
        float varSamplesY = computeVariance(this.samplesSensorY);
        float varSamplesZ = computeVariance(this.samplesSensorZ);

        Log.d(this.activityTag, String.format("Variances after %d samples are (x,y,z) = (%.2f, %.2f, %.2f)", NUM_SAMPLES_TO_CAPTURE, varSamplesX, varSamplesY, varSamplesZ));
        this.fireEvadedEvent(Math.abs(0.0f - varSamplesX) < EPSILON &&
                             Math.abs(0.0f - varSamplesY) < EPSILON &&
                             Math.abs(0.0f - varSamplesZ) < EPSILON);
        this.numSamples = 0;
    }

    private static float computeMean(float[] dataSet) {
        float sum = 0.0f;
        for(float dat : dataSet) {
            sum += dat;
        }

        return sum / dataSet.length;
    }

    private static float computeVariance(float[] dataSet) {
        float mean = computeMean(dataSet);
        float varSum = 0.0f;
        for(float dat : dataSet) {
            varSum += Math.pow(mean - dat, 2);
        }

        return varSum / dataSet.length;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
