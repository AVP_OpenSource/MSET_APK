package apks.cyberseclab.ca.et_battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_BATTERY");
    }

    private static final int EMULATOR_DEFAULT_BATTERY_LEVEL_1 = 50;
    private static final int EMULATOR_DEFAULT_BATTERY_LEVEL_2 = 0;

    @Override
    protected void executeEvasionDetection() {

        BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                Log.d((MainActivity.this).activityTag, String.format("Phone current battery level is %d", level));

                (MainActivity.this).fireEvadedEvent(level == EMULATOR_DEFAULT_BATTERY_LEVEL_1 || level == EMULATOR_DEFAULT_BATTERY_LEVEL_2);
            }
        };

        this.registerReceiver(batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }
}
