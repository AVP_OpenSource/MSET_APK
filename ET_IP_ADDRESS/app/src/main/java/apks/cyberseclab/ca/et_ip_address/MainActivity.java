package apks.cyberseclab.ca.et_ip_address;

import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.LinkedList;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {

    private static final String EMULATOR_DEFAULT_IP = "10.0.2.15";

    public MainActivity() {
        super("ET_IP_ADDRESS");
    }

    @Override
    protected void executeEvasionDetection() {
        LinkedList<String> ips = new LinkedList<>();
        try {
            for(Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for(Enumeration enIps = intf.getInetAddresses(); enIps.hasMoreElements();) {
                    String ipStr = ((InetAddress) enIps.nextElement()).getHostAddress().toString();
                    ips.add(ipStr);

                    Log.d(this.activityTag, String.format("Detected ip address %s", ipStr));
                }
            }

            this.fireEvadedEvent(ips.contains(EMULATOR_DEFAULT_IP));
        } catch (Exception e) {
            Log.e(this.activityTag, "Unable to fetch Network interfaces", e);
        }
    }
}
