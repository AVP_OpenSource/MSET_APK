package apks.cyberseclab.ca.et_call_logs;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {

    private static final int PERM_REQ_READ_CALL_LOGS = 11;

    private static final int MIN_CALL_COUNT = 5;


    public MainActivity() {
        super("ET_CALL_LOGS");
    }

    @Override
    protected void executeEvasionDetection() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_CALL_LOG
            }, PERM_REQ_READ_CALL_LOGS);
        }

        ContentResolver cr = getContentResolver();
        Cursor curCallLog = cr.query(CallLog.Calls.CONTENT_URI, null, null, null, null);

        int callLogCount = curCallLog.getCount();


        Log.d(this.activityTag, String.format("Current call log count is %d", callLogCount));

        this.fireEvadedEvent(callLogCount <= MIN_CALL_COUNT);
    }
}
