package apks.cyberseclab.ca.et_phone_no;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.regex.Pattern;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    private static final int PERM_REQ_PHONES_STATE = 10;
    private static final Pattern EMULATOR_PHONE_NO_PATT = Pattern.compile("^1?55552155([0-9]{2})$");

    public MainActivity() {
        super("ET_PHONE_NO");
    }

    @Override
    protected void executeEvasionDetection() {
        int permCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if(permCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_PHONE_STATE
            }, PERM_REQ_PHONES_STATE);
        }

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String devPhoneNo = tm.getLine1Number();
        Log.d(this.activityTag, String.format("Device phone no (line 1) is %s", devPhoneNo));
        this.fireEvadedEvent(EMULATOR_PHONE_NO_PATT.matcher(devPhoneNo).matches());
    }
}
