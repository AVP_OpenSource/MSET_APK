package apks.cyberseclab.ca.et_files;

import java.io.File;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_REAL_FILES");
    }

    private static final File Da = new File("/proc/uid_stat");
    private static final File Db = new File("/sys/devices/virtual/ppp");
    private static final File Dc = new File("/sys/devices/virtual/switch");
    private static final File Dd = new File("/sys/module/alarm/parameters");
    private static final File De = new File("/sys/devices/system/cpu/cpu0/cpufreq");
    private static final File Df = new File("/sys/devices/virtual/misc/android_adb");

    @Override
    protected void executeEvasionDetection() {
        this.fireEvadedEvent(!Da.exists() && !Db.exists() && !Dc.exists() && !Dd.exists() && !De.exists() && !Df.exists());
    }
}
