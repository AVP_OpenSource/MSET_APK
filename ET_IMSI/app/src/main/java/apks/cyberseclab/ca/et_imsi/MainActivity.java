package apks.cyberseclab.ca.et_imsi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    private static final int PERM_REQ_PHONES_STATE = 10;
    private static final String DEFAULT_EMULATOR_IMSI = "310260000000000";

    public MainActivity() {
        super("ET_IMSI");
    }

    @Override
    protected void executeEvasionDetection() {
        int permCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if(permCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_PHONE_STATE
            }, PERM_REQ_PHONES_STATE);
        }

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String devImsi = tm.getSubscriberId();
        Log.d(this.activityTag, String.format("Current IMSI is %s", devImsi));
        this.fireEvadedEvent(devImsi.equalsIgnoreCase(DEFAULT_EMULATOR_IMSI));
    }
}
