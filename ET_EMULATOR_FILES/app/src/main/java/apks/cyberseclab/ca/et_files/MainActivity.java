package apks.cyberseclab.ca.et_files;

import java.io.File;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

/**
 * Created by jeremiep on 2016-06-19.
 */
public class MainActivity extends ButtonTestingActivity {
    public MainActivity() {
        super("ET_EMULATOR_FILES");
    }

    private static final File Ea = new File("/proc/misc");
    private static final File Eb = new File("/proc/ioports");
    private static final File Ec = new File("/sys/devices/virtual/misc/cpu_dma_latency/uevent");
    private static final File Ed = new File("/proc/sys/net/ipv4/tcp_syncookies");

    @Override
    protected void executeEvasionDetection() {
        this.fireEvadedEvent(Ea.exists() && Eb.exists() && Ec.exists() && Ed.exists());
    }
}
